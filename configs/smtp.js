require('dotenv').config()

let smtpConfig = {
    host: process.env.MAILER_HOST ,
    port: process.env.MAILER_PORT ,
    authMethod: process.env.MAILER_AUTH_METHOD ,
    secure: true, // true for 465, false for other ports
    auth: {
        user: process.env.MAILER_AUTH_USERNAME, // your email address
        pass: process.env.MAILER_AUTH_PASSWORD, // your email password
    },
    logger: true,
    debug: true,
    ignoreTLS: false, // add this 
    requireTLS: true,
    tls: {
        // do not fail on invalid certs
        rejectUnauthorized: false,
    },
};

module.exports = smtpConfig