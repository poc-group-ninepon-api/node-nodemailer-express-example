require('dotenv').config()
const express = require('express')
const app = express()

const { sendEmail } = require('./src/mailer')

app.use(express.json());

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.post('/', async (req, res) => {
    const resultSendEMail =  await sendEmail(req.body)
    if(resultSendEMail){
        res.send('success')
    }else{
        res.status(504).send('fail')
    }
})
  
app.listen( process.env.SERVICE_PORT , () => {
    console.log(`service start on port ${process.env.SERVICE_PORT}`)
})