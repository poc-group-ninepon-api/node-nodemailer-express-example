require('dotenv').config()
const nodemailer = require('nodemailer');

const smtpConfig = require("../configs/smtp")
const tempA = require('./templates/tempA')


var transporter = nodemailer.createTransport(smtpConfig);

async function sendEmail(mailOptions) {
    const resVerify = await transporter.verify();
    if(!resVerify){
        return false
    }

    // // setup email data with unicode symbols
    // let mailOptions = {
    //     from: '' , // sender address
    //     to: '', // list of receivers
    //     subject: '', // Subject line
    //     text: '', // plain text body
    //     html: '' // html body
    // };
    mailOptions.html = tempA
    try{
        const resSendEmail = await transporter.sendMail(mailOptions)
        console.log(resSendEmail)
        return true
    }catch(err){
        console.error(err)
        return false
    }
}

module.exports = {
    sendEmail
}