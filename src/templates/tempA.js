const tempA = `
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>My Email Template</title>
</head>
<body>
	<div style="background-color: #f9f9f9; padding: 20px;">
		<h1 style="color: #333;">Hello!</h1>
		<p style="color: #666; font-size: 16px;">Thank you for subscribing to our newsletter.</p>
		<p style="color: #666; font-size: 16px;">Here's some useful information for you:</p>
		<ul style="color: #666; font-size: 16px;">
			<li>Our latest product updates</li>
			<li>Tips and tricks for using our products</li>
			<li>Exclusive promotions and discounts</li>
		</ul>
		<p style="color: #666; font-size: 16px;">If you have any questions or feedback, please don't hesitate to contact us.</p>
		<p style="color: #666; font-size: 16px;">Best regards,</p>
		<p style="color: #666; font-size: 16px;">The My Company Team</p>
	</div>
</body>
</html>
`

module.exports = tempA